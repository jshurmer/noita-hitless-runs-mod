# Noita Hitless Runs

This is a mod for noita which adds some useful things for doing hitless runs.

[Available on Steam!](https://steamcommunity.com/sharedfiles/filedetails/?id=2308040016)

# Features

* Any damage taken will kill you instantly
* Enable or Disable in the Mod Settings while playing. no restart needed

# Installing

Install it through the Steam Workshop: [Hitless Runs](https://steamcommunity.com/sharedfiles/filedetails/?id=2308040016)

## From source

* Clone this repo into your `Noita/mods` folder
  * Steam installs the game to `C:\Program Files (x86)\Steam\steamapps\common` by default

```
cd 'C:\Program Files (x86)\Steam\steamapps\common\Noita\mods\'
git clone git@gitlab.com:jshurmer/noita-hitless-runs-mod.git
```


# Roadmap

Here's what I'm thinking for where this mod is going:

* Show "Hitless Mode: On" on the new game menu (like Streamer mode)
* Configure how many floors you want to do hitless. It will auto-disable after that point.
  * This lets you get good practice, but then play the normal game after a certain point
  * So, you can do a hitless first-floor run for example.
* Add a Perk indicating that you're playing hitless
  * Perk would go away when it's disabled
  * Maybe use McQueeb's beard icon as the perk icon?
* Display a text like the Biome Modifier when the game starts that says something like "You feel very unsafe"

Any other ideas? Submit an Issue here on gitlab
