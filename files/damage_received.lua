dofile_once( "data/scripts/lib/utilities.lua" )

-- damage_received( damage:number, message:string, entity_thats_responsible:int, is_fatal:bool, projectile_thats_responsible:int )
function damage_received(damage, message, entity_thats_responsible, is_fatal)
    
    local enabled = ModSettingGet("hitless_runs.enabled")
    if enabled then
        -- get all the damagemodels affecting the player
        local damagemodels = EntityGetComponent( get_players()[1], "DamageModelComponent" )
        if( damagemodels ~= nil ) then
            -- force the first damagemodel to kill the player
            ComponentSetValue2( damagemodels[1], "hp", 0 )
        end
    end
end

