dofile_once("data/scripts/lib/utilities.lua")

function OnPlayerSpawned( player_entity )
	local enabled = ModSettingGet("hitless_runs.enabled")

	if enabled then
		GamePrint("Hitless Run")
	end

	EntityAddComponent(
		player_entity, "LuaComponent",
		{ _enabled="1" , script_damage_received="mods/hitless_runs/files/damage_received.lua", }
		-- script_damage_received "if set, calls function 'damage_received( damage:number, message:string, entity_thats_responsible:int, is_fatal:bool, projectile_thats_responsible:int )' when we receive a message about damage (Message_DamageReceived)"
	)
	
end
