-- include the games mod settings functionality
dofile("data/scripts/lib/mod_settings.lua")

-- Use ModSettingGet() in the game to query settings.

local mod_id = "hitless_runs"
mod_settings_version = 1 -- magic number needed by noita
mod_settings = 
{
	{
		id = "_",
		ui_name = "",
		not_setting = true,
	},
	{
		id = "enabled",
		ui_name = "Enabled",
		value_default = true,
		scope = MOD_SETTING_SCOPE_RUNTIME,
		foldable = true,
		_folded = true
	}
}

function ModSettingsUpdate( init_scope )
	mod_settings_update( mod_id, mod_settings, init_scope )
end

function ModSettingsGuiCount()
	return mod_settings_gui_count( mod_id, mod_settings )
end

function ModSettingsGui( gui, in_main_menu )
	mod_settings_gui( mod_id, mod_settings, gui, in_main_menu )
end
